FROM maven:3.6.3-openjdk-11-slim AS builder
COPY . .
RUN mvn install -Dmaven.test.skip=true --quiet

FROM openjdk:11.0.9-slim
COPY --from=builder /target/*.jar ./app.jar
ENTRYPOINT ["java", "-jar", "app.jar"]
