create table conference(
    id BIGSERIAL PRIMARY KEY,
    end_date date not null ,
    name varchar(255) not null,
    participant_amount int4 not null,
    start_date date not null,
    subject varchar(255)
                       );

create table talk (
    id BIGSERIAL PRIMARY KEY,
    name varchar(255) not null,
    report_type varchar(255) not null,
    conference_id int8,
    speaker_id int8
                    );

create table speaker (
    id BIGSERIAL PRIMARY KEY,
    first_name varchar(255),
    last_name varchar(255),
    title varchar(255)
                     );

alter table if exists talk add constraint fk_conference foreign key (conference_id) references conference;
alter table if exists talk add constraint fk_speaker foreign key (speaker_id) references speaker;

