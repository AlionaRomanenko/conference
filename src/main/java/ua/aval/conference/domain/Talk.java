package ua.aval.conference.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import ua.aval.conference.dto.TalkDTO;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.function.Function;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Talk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull
    private String name;

    @Enumerated(EnumType.STRING)
    private ReportType reportType;

    @ManyToOne
    @JoinColumn(name = "speaker_id")
    private Speaker speaker;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "conference_id")
    private Conference conference;


    public static final Function<Talk, TalkDTO> talk2TalkDTO = talk -> new TalkDTO(
            talk.name,
            talk.reportType.name(),
            talk.speaker

    );

    public static final Function<TalkDTO, Talk> talkDTO2Talk = t -> {

        final Talk talk = new Talk();
        talk.setName(t.getName());
        talk.setReportType(ReportType.valueOf(t.getReportType()));
        if (t.getSpeaker() != null) {
            talk.setSpeaker(t.getSpeaker());
        }
        return talk;
    };
}
