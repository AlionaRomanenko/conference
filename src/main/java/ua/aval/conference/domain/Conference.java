package ua.aval.conference.domain;

import lombok.*;
import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.ConferenceUpdateGetDTO;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

@Getter
@Setter
@NoArgsConstructor
@Entity
@EqualsAndHashCode
public class Conference {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    @NotNull(message = "Name should not be empty")
    private String name;

    @NotNull(message = "Subject should not be empty")
    private String subject;

    @NotNull(message = "StartDate should not be empty")
    private LocalDate startDate;

    @NotNull(message = "EndDate should not be empty")
    private LocalDate endDate;

    @NotNull(message = "Amount of participants should not be empty")
    @Min(100)
    private Integer participantAmount;

    @OneToMany(cascade = CascadeType.ALL,
    orphanRemoval = true, mappedBy = "conference", fetch = FetchType.LAZY)
    private List<Talk> talks;


    public static final Function<Conference, ConferenceUpdateGetDTO> conf2ConfUpdateDTO = conf -> new ConferenceUpdateGetDTO(
            conf.id,
            conf.name,
            conf.subject,
            conf.startDate,
            conf.endDate,
            conf.participantAmount
    );

    public static final Function<Conference, ConferenceDTO> conf2ConfDTO = conf -> new ConferenceDTO(
            conf.name,
            conf.subject,
            conf.startDate,
            conf.endDate,
            conf.participantAmount
    );

    public static final Function<ConferenceDTO, Conference> confDTO2Conf = conf -> {

        final Conference conference = new Conference();
        conference.setName(conf.getName());
        conference.setSubject(conf.getSubject());
        conference.setStartDate(conf.getStartDate());
        conference.setEndDate(conf.getEndDate());
        conference.setParticipantAmount(conf.getParticipantAmount());
        return conference;
    };

    public static final Function<ConferenceUpdateGetDTO, Conference> confUpdateDTO2Conf = conf -> {

        final Conference conference = new Conference();
        conference.setId(conf.getId());
        conference.setName(conf.getName());
        conference.setSubject(conf.getSubject());
        conference.setStartDate(conf.getStartDate());
        conference.setEndDate(conf.getEndDate());
        conference.setParticipantAmount(conf.getParticipantAmount());
        return conference;
    };
}
