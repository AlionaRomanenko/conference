package ua.aval.conference.service;

import ua.aval.conference.dto.TalkDTO;

import java.util.List;

public interface TalkService {

    List<TalkDTO> getAllReportsByConference(Long id);

    TalkDTO createReportByConference(Long id, TalkDTO report);
}
