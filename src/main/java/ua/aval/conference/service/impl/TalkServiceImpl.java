package ua.aval.conference.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ua.aval.conference.domain.Conference;
import ua.aval.conference.domain.Talk;
import ua.aval.conference.domain.ReportType;
import ua.aval.conference.dto.TalkDTO;
import ua.aval.conference.exception.*;
import ua.aval.conference.repository.ConferenceRepository;
import ua.aval.conference.repository.TalkRepository;
import ua.aval.conference.service.TalkService;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
public class TalkServiceImpl implements TalkService {

    private static final int MAX_REPORT_AMOUNT = 3;
    private static final int SUBMIT_REPORT_MONTHS = 1;

    private final TalkRepository repository;
    private final ConferenceRepository conferenceRepository;

    public List<TalkDTO> getAllReportsByConference(Long id) {
        log.info("Getting talks by conference with id " + id);
        return repository.findAllByConference_Id(id).stream().map(Talk.talk2TalkDTO).collect(Collectors.toList());
    }


    public TalkDTO createReportByConference(Long id, TalkDTO talkDTO) {
        log.info("Creating talk with name {} for conference with id {}", talkDTO.getName(), id);
        Set<String> enumNames = new HashSet<>();
        for (ReportType keyValue : ReportType.values()){
            enumNames.add(keyValue.name());
        }

        if(!enumNames.contains(talkDTO.getReportType())){
            throw new NotValidReportTypeException("ReportType " + talkDTO.getReportType() + "does not exist");
        }
        Talk talk = Talk.talkDTO2Talk.apply(talkDTO);
        Conference conference = conferenceRepository.findById(id).orElseThrow(() ->
                new NotFoundInDatabaseException(String.format("Conference with id %s not found", id)));
        if (repository.findAllByName(talkDTO.getName()).isPresent()) {
            throw new TalkNameShouldBeUniqueException(String.format("Talk with name %s already exists", talk.getName()));
        }
        if (talk.getSpeaker() != null && repository.findAllBySpeakerId(talk.getSpeaker().getId()).size() > MAX_REPORT_AMOUNT) {
            throw new ToMuchReportsForOneSpeakerException(String.format("Speaker with id %s can not make this talk, because he already has %s talks ", talk.getSpeaker().getId(), MAX_REPORT_AMOUNT));
        }
        if (LocalDate.now().plusMonths(SUBMIT_REPORT_MONTHS).isAfter(conference.getStartDate())) {
            throw new ConferenceDatesOverlapException(String.format("Adding new talks to conference should be at least for %s months in advance ", SUBMIT_REPORT_MONTHS));
        }
        talk.setConference(conference);
        return Talk.talk2TalkDTO.apply(repository.save(talk));
    }


}
