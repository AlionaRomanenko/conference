package ua.aval.conference.service.impl;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ua.aval.conference.config.MetricConfig;
import ua.aval.conference.domain.Conference;
import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.ConferenceUpdateGetDTO;
import ua.aval.conference.exception.ConferenceDatesOverlapException;
import ua.aval.conference.exception.ConferenceNameShouldBeUniqueException;
import ua.aval.conference.exception.ConferenceStartDateIsAfterEndDateException;
import ua.aval.conference.repository.ConferenceRepository;
import ua.aval.conference.service.ConferenceService;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class ConferenceServiceImpl implements ConferenceService {


    private final ConferenceRepository conferenceRepository;
    private final MetricConfig metricConfig;

    public ConferenceServiceImpl(ConferenceRepository conferenceRepository, MetricConfig metricConfig, MeterRegistry meterRegistry) {
        this.conferenceRepository = conferenceRepository;
        this.metricConfig = metricConfig;
    }

    @Override
    public List<ConferenceUpdateGetDTO> getAll() {
        log.info("Getting all conferences");
        return conferenceRepository.findAll().stream().map(Conference.conf2ConfUpdateDTO).collect(Collectors.toList());
    }



    @Override
    @Timed(description = "Time spent creating conference")
    public ConferenceDTO createConference(ConferenceDTO conferenceDTO) {

        log.info("Creating/updating conference with name " + conferenceDTO.getName());
        Conference conference = Conference.confDTO2Conf.apply(conferenceDTO);
        if (conferenceRepository.getAllByName(conference.getName()).isPresent()) {
            throw new ConferenceNameShouldBeUniqueException(String.format("Conference with name %s already exists", conference.getName()));
        }
        log.info("Check if conference with name {} overlap dates with other conferences ", conferenceDTO.getName());
        crossDateCheck(conference.getStartDate(), conference.getEndDate());
        return Conference.conf2ConfDTO.apply(conferenceRepository.save(conference));
    }

    @Override
    public ConferenceUpdateGetDTO updateConference(ConferenceUpdateGetDTO conferenceRequest) {
        log.info("Updating conference with id " + conferenceRequest.getId());
        if(!conferenceRepository.getAllByNameAndIdNotEquals(conferenceRequest.getName(), conferenceRequest.getId()).isEmpty()){
            throw new ConferenceNameShouldBeUniqueException("Conference with name " + conferenceRequest.getName() + "already exist");
        }

        if(conferenceRequest.getStartDate().isAfter(conferenceRequest.getEndDate())){
            throw new ConferenceStartDateIsAfterEndDateException("StartDate of conference should be before endDate");
        }
        Conference conference = Conference.confUpdateDTO2Conf.apply(conferenceRequest);
        log.info("Check if conference with name {} overlap dates with other conferences ", conferenceRequest.getName());
        crossDateCheck(conference.getStartDate(), conference.getEndDate());
        return Conference.conf2ConfUpdateDTO.apply(conferenceRepository.save(conference));
    }

    private void crossDateCheck(LocalDate startDate, LocalDate endDate) {

        if (conferenceRepository.findCrossDate(startDate, endDate).isPresent()) {
            metricConfig.incrementCountOfCustomExceptionMetric(1.0);
            throw new ConferenceDatesOverlapException("Dates of conference overlap with dates of another conference");
        }
    }


}
