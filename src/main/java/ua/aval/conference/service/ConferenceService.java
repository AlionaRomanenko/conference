package ua.aval.conference.service;

import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.ConferenceUpdateGetDTO;

import java.util.List;

public interface ConferenceService {

    ConferenceDTO createConference(ConferenceDTO conference);

    ConferenceUpdateGetDTO updateConference(ConferenceUpdateGetDTO conference);

    List<ConferenceUpdateGetDTO> getAll();

}
