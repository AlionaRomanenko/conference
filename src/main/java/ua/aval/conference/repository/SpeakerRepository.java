package ua.aval.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.aval.conference.domain.Speaker;

@Repository
public interface SpeakerRepository extends JpaRepository<Speaker, Long> {
}
