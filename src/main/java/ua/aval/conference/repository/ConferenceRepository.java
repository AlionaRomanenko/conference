package ua.aval.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ua.aval.conference.domain.Conference;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ConferenceRepository extends JpaRepository<Conference, Long> {

    @Query("select conf from Conference conf where  not ((conf.endDate < ?1 and conf.startDate < ?1) or (conf.endDate > ?2 and conf.startDate > ?2))")
    Optional<Conference> findCrossDate(LocalDate startDate, LocalDate endDate);

    Optional<Conference> getAllByName(String name);

    @Query("select conf from Conference conf where  conf.id <> ?2 and conf.name = ?1")
    List<Conference> getAllByNameAndIdNotEquals(String name,Long id);
}
