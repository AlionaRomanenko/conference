package ua.aval.conference.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.aval.conference.domain.Talk;

import java.util.List;
import java.util.Optional;

@Repository
public interface TalkRepository extends JpaRepository<Talk, Long> {

    List<Talk> findAllByConference_Id(Long id);

    Optional<Talk> findAllByName(String name);

    List<Talk> findAllBySpeakerId(Long id);
}
