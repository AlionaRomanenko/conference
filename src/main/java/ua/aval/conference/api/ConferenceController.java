package ua.aval.conference.api;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.ConferenceUpdateGetDTO;
import ua.aval.conference.dto.TalkDTO;
import ua.aval.conference.service.ConferenceService;
import ua.aval.conference.service.TalkService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/conferences")
@RequiredArgsConstructor
public class ConferenceController {

    private final ConferenceService conferenceService;

    private final TalkService talkService;


    @GetMapping("/{conferenceId}/talks")
    public List<TalkDTO> getAllTalksByConference(@PathVariable Long conferenceId) {

        return talkService.getAllReportsByConference(conferenceId);

    }

    @PostMapping("/{conferenceId}/talks")
    public TalkDTO createTalkForConference(@PathVariable Long conferenceId, @Valid @RequestBody TalkDTO talkDTO) {

        return talkService.createReportByConference(conferenceId, talkDTO);

    }

    @PutMapping("/{conferenceId}")
    public ConferenceUpdateGetDTO updateConference(@Valid @RequestBody ConferenceUpdateGetDTO conference){

        return conferenceService.updateConference(conference);
    }

    @GetMapping
    public List<ConferenceUpdateGetDTO> getConferences() {

        return conferenceService.getAll();
    }


    @PostMapping
    public ConferenceDTO createConference(@Valid @RequestBody ConferenceDTO conference) {

        return conferenceService.createConference(conference);

    }

}
