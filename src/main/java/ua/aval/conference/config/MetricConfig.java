package ua.aval.conference.config;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class MetricConfig {

    private MeterRegistry meterRegistry;
    private Counter exceptionCounter;

    public MetricConfig(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }


    @PostConstruct
    private void creteCounter() {
        exceptionCounter = Counter.builder("custom.exception")
                .tag("type", "custom")
                .description("The number of calls of custom ConferenceDatesOverlapException")
                .register(meterRegistry);
    }


    public void incrementCountOfCustomExceptionMetric(Double increment) {
        exceptionCounter.increment(increment);
        }
    }
