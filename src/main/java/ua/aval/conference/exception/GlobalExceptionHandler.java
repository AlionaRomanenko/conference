package ua.aval.conference.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler({UniqueNameException.class, NotFoundInDatabaseException.class})
    public ResponseEntity handleConflictException(RuntimeException e) {
        log.error(e.getMessage());
        return new ResponseEntity(new ApiError(HttpStatus.CONFLICT, e.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler({ConferenceDatesOverlapException.class, ToMuchReportsForOneSpeakerException.class, NotValidReportTypeException.class})
    public ResponseEntity handleBadRequestException(RuntimeException e) {
        log.error(e.getMessage());
        return new ResponseEntity(new ApiError(HttpStatus.BAD_REQUEST, e.getMessage()), HttpStatus.BAD_REQUEST);
    }
}
