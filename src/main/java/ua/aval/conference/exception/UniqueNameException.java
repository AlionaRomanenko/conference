package ua.aval.conference.exception;

public class UniqueNameException extends RuntimeException {

    public UniqueNameException(String message) {
        super(message);
    }
}
