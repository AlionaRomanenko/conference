package ua.aval.conference.exception;

public class TalkNameShouldBeUniqueException extends UniqueNameException {
    public TalkNameShouldBeUniqueException(String message) {
        super(message);
    }
}
