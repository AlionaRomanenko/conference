package ua.aval.conference.exception;

public class ToMuchReportsForOneSpeakerException extends RuntimeException {

    public ToMuchReportsForOneSpeakerException(String message) {
        super(message);
    }
}
