package ua.aval.conference.exception;

public class ConferenceStartDateIsAfterEndDateException extends RuntimeException {

    public ConferenceStartDateIsAfterEndDateException(String message){
        super(message);
    }
}
