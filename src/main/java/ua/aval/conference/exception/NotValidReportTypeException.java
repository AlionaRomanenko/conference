package ua.aval.conference.exception;

public class NotValidReportTypeException extends RuntimeException{
    public NotValidReportTypeException(String message) {
        super(message);
    }
}
