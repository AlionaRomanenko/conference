package ua.aval.conference.exception;

public class ConferenceNameShouldBeUniqueException extends UniqueNameException {

    public ConferenceNameShouldBeUniqueException(String message) {
        super(message);
    }
}
