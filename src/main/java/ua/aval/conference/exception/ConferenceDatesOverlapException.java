package ua.aval.conference.exception;

public class ConferenceDatesOverlapException extends RuntimeException {

    public ConferenceDatesOverlapException(String message) {
        super(message);
    }
}
