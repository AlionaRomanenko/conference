package ua.aval.conference.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.aval.conference.domain.Speaker;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TalkDTO {

    @NotNull
    private String name;

    @NotNull
    private String reportType;

    private Speaker speaker;


}
