package ua.aval.conference.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConferenceUpdateGetDTO {

    @NotNull
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String subject;

    @NotNull
    private LocalDate startDate;

    @NotNull
    private LocalDate endDate;

    @NotNull
    @Min(100)
    private Integer participantAmount;


}
