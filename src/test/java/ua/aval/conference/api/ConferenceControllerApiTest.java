package ua.aval.conference.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.database.rider.core.DBUnitRule;
import com.github.database.rider.core.api.dataset.DataSet;
import io.restassured.RestAssured;
import lombok.SneakyThrows;
import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;
import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.TalkDTO;

import javax.sql.DataSource;
import java.time.LocalDate;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ConferenceControllerApiTest {

    @Autowired
    private WebApplicationContext context;

    @LocalServerPort
    private int port;

    @Before
    public void init() {
        RestAssured.port = port;

    }

    @Autowired
    private ObjectMapper objectMapper;

    @Rule
    public DBUnitRule dbUnitRule = DBUnitRule.instance("system",
            () -> context.getBean(DataSource.class).getConnection());

    @Test
    @DataSet("get-talks.yml")
    public void canGetAllConferences() {

        List<String> conferenceDTOList = given()
                .contentType("application/json")
                .when()
                .get("/conferences")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType("application/json")
                .extract().body().jsonPath().getList("name");
        assertThat(conferenceDTOList).hasSize(2)
                .contains("test");
    }

    @SneakyThrows
    @Test
    @DataSet("get-talks.yml")
    public void wrongConferenceUpdateWhenViolatesUniqueNameConstraint() {
        ConferenceDTO conference = new ConferenceDTO();
        conference.setName("test");
        conference.setStartDate(LocalDate.now());
        conference.setEndDate(LocalDate.now().plusMonths(1L));
        conference.setParticipantAmount(110);
        conference.setSubject("REPORT");
        String body = objectMapper.writeValueAsString(conference);
        given()
                .contentType("application/json")
                .body(body)
                .when()
                .post("/conferences")
                .then()
                .statusCode(HttpStatus.SC_CONFLICT)
                .contentType("application/json");
    }


    @SneakyThrows
    @Test
    @DataSet({"get-talks.yml"})
    public void wrongTalkUpdateWhenReportTypeisNotCorrect() {
        TalkDTO talkDTO = new TalkDTO();
        talkDTO.setName("test1");
        talkDTO.setReportType("gtgyh");

        String body = objectMapper.writeValueAsString(talkDTO);
        given()
                .contentType("application/json")
                .body(body)
                .when()
                .post("/conferences/2/talks")
                .then()
                .statusCode(HttpStatus.SC_BAD_REQUEST)
                .contentType("application/json");
    }

    @SneakyThrows
    @Test
    @DataSet({"get-talks.yml"})
    public void OKTalkCreate() {
        TalkDTO talkDTO = new TalkDTO();
        talkDTO.setName("report");
        talkDTO.setReportType("REPORT");

        String body = objectMapper.writeValueAsString(talkDTO);
        String dto = given()
                .contentType("application/json")
                .body(body)
                .when()
                .post("/conferences/2/talks")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType("application/json")
                .extract().body().jsonPath().get("name");
        assertThat(dto).isEqualTo("report");

    }

    @Test
    @DataSet({"get-talks.yml"})
    public void canGetAllTalksByConference() {

        List<String> names = given()
                .contentType("application/json")
                .when()
                .get("/conferences/2/talks")
                .then()
                .statusCode(HttpStatus.SC_OK)
                .contentType("application/json")
                .extract().body().jsonPath().getList("name");
        assertThat(names).contains("test_report")
                .doesNotContain("test_rep");
    }
}
