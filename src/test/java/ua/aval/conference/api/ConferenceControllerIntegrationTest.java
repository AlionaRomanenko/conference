package ua.aval.conference.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.ConferenceUpdateGetDTO;
import ua.aval.conference.dto.TalkDTO;
import ua.aval.conference.service.ConferenceService;
import ua.aval.conference.service.TalkService;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(ConferenceController.class)
class ConferenceControllerIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ConferenceService conferenceService;

    @MockBean
    private TalkService talkService;


    @Test
    void createConferenceWhenInputFieldsAreNull() throws Exception {
        String body = new ObjectMapper().writeValueAsString(createConferenceDtoWithNameOnly());
        this.mvc.perform(post("/conferences")
                .contentType("application/json")
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateConferenceWhenParticipantAmountLessThen100() throws Exception {
        ConferenceUpdateGetDTO conference = createValidConferenceUpdateGetDto();
        conference.setParticipantAmount(80);
        String body = new ObjectMapper().writeValueAsString(conference);
        this.mvc.perform(put("/conferences/1")
                .contentType("application/json")
                .content(body))
                .andExpect(status().isBadRequest());
    }

    @Test
    void updateConferenceWhenEverythingIsCorrect() throws Exception {
        ConferenceUpdateGetDTO conference = createValidConferenceUpdateGetDto();
        String body = new ObjectMapper().writeValueAsString(conference);

        given(conferenceService.updateConference(conference)).willReturn(conference);
        this.mvc.perform(put("/conferences/1")
                .contentType("application/json")
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("participantAmount", is(conference.getParticipantAmount())));
    }


    @Test
    public void getAllConferences() throws Exception {
        ConferenceUpdateGetDTO conferenceDTO = createValidConferenceUpdateGetDto();
        List<ConferenceUpdateGetDTO> conferenceDTOS = Collections.singletonList(conferenceDTO);
        given(conferenceService.getAll()).willReturn(conferenceDTOS);
        this.mvc.perform(get("/conferences"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(conferenceDTO.getName())));
    }

    @Test
    public void unexpectedRestMethodCheck() throws Exception {
        this.mvc.perform(put("/conferences"))
                .andExpect(status().isMethodNotAllowed());
    }

    @Test
    void createReportWhenReportTypeisNull() throws Exception {
        TalkDTO talk = createValidTalk();
        talk.setReportType(null);
        String body = new ObjectMapper().writeValueAsString(talk);
        this.mvc.perform(post("/conferences/1/talks")
                .contentType("application/json")
                .content(body))
                .andExpect(status().isBadRequest());
    }


    @Test
    void createTalkWhenEverythingIsCorrect() throws Exception {
        TalkDTO talkDTO = createValidTalk();
        String body = new ObjectMapper().writeValueAsString(talkDTO);

        given(talkService.createReportByConference(1L, talkDTO)).willReturn(talkDTO);
        this.mvc.perform(post("/conferences/1/talks")
                .contentType("application/json")
                .content(body))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name", is(talkDTO.getName())));
    }


    @Test
    public void getAllReportsByConferences() throws Exception {
        TalkDTO talkDTO = new TalkDTO();
        talkDTO.setName("talk");
        List<TalkDTO> talkDTOS = Collections.singletonList(talkDTO);
        given(talkService.getAllReportsByConference(1L)).willReturn(talkDTOS);
        this.mvc.perform(get("/conferences/1/talks"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].name", is(talkDTO.getName())));
    }

    public TalkDTO createValidTalk() {
        TalkDTO talkDTO = new TalkDTO();
        talkDTO.setName("myTalk");
        talkDTO.setReportType("REPORT");
        return talkDTO;
    }

    public ConferenceDTO createConferenceDtoWithNameOnly() {
        ConferenceDTO conferenceDTO = new ConferenceDTO();
        conferenceDTO.setName("name");
        return conferenceDTO;
    }

    public ConferenceUpdateGetDTO createValidConferenceUpdateGetDto() {
        ConferenceUpdateGetDTO conference = new ConferenceUpdateGetDTO();
        conference.setStartDate(LocalDate.now());
        conference.setEndDate(LocalDate.now().plusMonths(1L));
        conference.setId(1L);
        conference.setParticipantAmount(120);
        conference.setSubject("REPORT");
        conference.setName("conf");
        return conference;
    }
}
