package ua.aval.conference.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import ua.aval.conference.domain.Conference;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ConferenceRepositoryTest {

    @Autowired
    private ConferenceRepository repository;

    @Before
    public void init() {

        Conference conference = new Conference();
        conference.setName("name");
        conference.setSubject("subject");
        conference.setParticipantAmount(130);
        conference.setStartDate(LocalDate.now());
        conference.setEndDate(LocalDate.now().plusDays(8));
        repository.save(conference);

    }

    @Test
    public void checkCrossDateWhenDateDoesntOverlap() {
        assertEquals(repository.findCrossDate(LocalDate.now().plusYears(1), LocalDate.now().plusDays(3).minusYears(1)), Optional.empty());
    }

    @Test
    public void checkCrossDatesWhenStartDateIsRightAfterEndOfExistingConf() {

        assertThat(repository.findCrossDate(LocalDate.now().plusDays(8), LocalDate.now().plusDays(18)).get().getName()).isEqualTo("name");
    }

    @Test
    public void checkCrossDateWhenDatesOverlap() {
        assertEquals("name", repository.findCrossDate(LocalDate.now().plusDays(1), LocalDate.now().plusDays(3)).get().getName());
    }

    @Test
    public void checkGetAllByNameAndIdNotEqualsRequest() {
        assertThat(repository.getAllByNameAndIdNotEquals("name", 8L)).hasSize(1);
    }


}
