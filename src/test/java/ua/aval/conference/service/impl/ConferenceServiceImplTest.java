package ua.aval.conference.service.impl;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.aval.conference.config.MetricConfig;
import ua.aval.conference.domain.Conference;
import ua.aval.conference.dto.ConferenceDTO;
import ua.aval.conference.dto.ConferenceUpdateGetDTO;
import ua.aval.conference.exception.ConferenceDatesOverlapException;
import ua.aval.conference.exception.ConferenceNameShouldBeUniqueException;
import ua.aval.conference.exception.ConferenceStartDateIsAfterEndDateException;
import ua.aval.conference.repository.ConferenceRepository;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ConferenceServiceImplTest {

    @Mock
    private ConferenceRepository conferenceRepository;

    @Mock
    private MetricConfig metricConfig;
    @InjectMocks
    private ConferenceServiceImpl service;

    @Spy
    private Conference conference;




    @Test
    public void createConferenceIfStartDateIsAfterEndDateException() {
        ConferenceUpdateGetDTO conferenceDTO = new ConferenceUpdateGetDTO();
        conferenceDTO.setEndDate(LocalDate.now());
        conferenceDTO.setStartDate(LocalDate.now().plusMonths(1));
        assertThrows(ConferenceStartDateIsAfterEndDateException.class, () -> { service.updateConference(conferenceDTO);
        });
    }

    @Test
    public void updateConferenceIfViolatesUniqueConferenceName(){
        ConferenceUpdateGetDTO conferenceDTO = new ConferenceUpdateGetDTO();
        conferenceDTO.setName("name");
        conferenceDTO.setId(1L);
        conference.setId(1L);
        List<Conference> conferences = Arrays.asList(conference);
        when(conferenceRepository.getAllByNameAndIdNotEquals(conferenceDTO.getName(), conference.getId())).thenReturn(conferences);
        assertThrows(ConferenceNameShouldBeUniqueException.class, () -> { service.updateConference(conferenceDTO);
        });
    }

    @Test
    public void createConferenceIfViolatesUniqueConferenceName(){
        ConferenceDTO conferenceDTO = new ConferenceDTO();
        conferenceDTO.setName("name");
        when(conferenceRepository.getAllByName(conferenceDTO.getName())).thenReturn(Optional.of(conference));
        assertThrows(ConferenceNameShouldBeUniqueException.class, () -> { service.createConference(conferenceDTO);
        });
    }

    @Test
    public void createConferenceIfDatesOverlapWithAnotherConference(){
        ConferenceDTO conferenceDTO = new ConferenceDTO();
        conferenceDTO.setName("name");
        conferenceDTO.setStartDate(LocalDate.of(2020, 12,12));
        conferenceDTO.setEndDate(LocalDate.of(2020,12,25));
        when(conferenceRepository.getAllByName(conferenceDTO.getName())).thenReturn(Optional.empty());
        when(conferenceRepository.findCrossDate(conferenceDTO.getStartDate(), conferenceDTO.getEndDate())).thenReturn(Optional.of(conference));
        doNothing().when(metricConfig).incrementCountOfCustomExceptionMetric(1.0);
        assertThrows(ConferenceDatesOverlapException.class, () -> { service.createConference(conferenceDTO);
        });
    }

    @Test
    public void createConferenceWhenAllIsOk(){
        ConferenceDTO conferenceDTO = new ConferenceDTO();
        conferenceDTO.setName("name");
        conferenceDTO.setStartDate(LocalDate.of(2020, 12,12));
        conferenceDTO.setEndDate(LocalDate.of(2020,12,25));
        Conference conference1 = Conference.confDTO2Conf.apply(conferenceDTO);
        when(conferenceRepository.getAllByName(conferenceDTO.getName())).thenReturn(Optional.empty());
        when(conferenceRepository.findCrossDate(conferenceDTO.getStartDate(), conferenceDTO.getEndDate())).thenReturn(Optional.empty());
        when(conferenceRepository.save(conference1)).thenReturn(conference1);
        assertEquals(service.createConference(conferenceDTO),conferenceDTO);

    }


}
