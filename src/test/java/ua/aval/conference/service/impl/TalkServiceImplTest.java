package ua.aval.conference.service.impl;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ua.aval.conference.domain.Conference;
import ua.aval.conference.domain.Talk;
import ua.aval.conference.dto.TalkDTO;
import ua.aval.conference.exception.ConferenceDatesOverlapException;
import ua.aval.conference.exception.NotFoundInDatabaseException;
import ua.aval.conference.exception.NotValidReportTypeException;
import ua.aval.conference.exception.TalkNameShouldBeUniqueException;
import ua.aval.conference.repository.ConferenceRepository;
import ua.aval.conference.repository.TalkRepository;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class TalkServiceImplTest {

    @Mock
    private TalkRepository repository;

    @Mock
    private ConferenceRepository conferenceRepository;

    @InjectMocks
    private TalkServiceImpl service;


    @Spy
    private Talk talk;


    @Test
    void createTalkWhenNameISNotUnique() {
        TalkDTO talkDTO = createValidTalk();
        Conference conference = new Conference();
        conference.setName("conference_name");
        conference.setId(1L);
        talk.setName("name");
        Mockito.when(conferenceRepository.findById(1L)).thenReturn(Optional.of(conference));
        Mockito.when(repository.findAllByName(talkDTO.getName())).thenReturn(Optional.of(talk));
        assertThrows(TalkNameShouldBeUniqueException.class, () -> {
            service.createReportByConference(1L, talkDTO);
        });
    }

    @Test
    void createTalkWhenConferenceIsLessThenInOneMonth() {
        TalkDTO talkDTO = createValidTalk();
        Conference conference = new Conference();
        conference.setName("conference_name");
        conference.setId(1L);
        conference.setStartDate(LocalDate.now().plusDays(15));
        Mockito.when(conferenceRepository.findById(1L)).thenReturn(Optional.of(conference));
        Mockito.when(repository.findAllByName(talkDTO.getName())).thenReturn(Optional.empty());
        assertThrows(ConferenceDatesOverlapException.class, () -> {
            service.createReportByConference(1L, talkDTO);
        });
    }

    @Test
    void createTalkWhenConferenceDoesNotExist() {
        Conference conference = new Conference();
        conference.setName("conference_name");
        conference.setId(1L);
        conference.setStartDate(LocalDate.now().plusDays(15));
        Mockito.when(conferenceRepository.findById(1L)).thenReturn(Optional.empty());
        assertThrows(NotFoundInDatabaseException.class, () -> {
            service.createReportByConference(1L, createValidTalk());
        });
    }

    @Test
    void createTalkWhenReportTypeISInvalid() {
        TalkDTO talkDTO = createValidTalk();
        talkDTO.setReportType("fguhj");
        Conference conference = new Conference();
        conference.setName("conference_name");
        conference.setId(1L);
        conference.setStartDate(LocalDate.now().plusMonths(2));
        assertThrows(NotValidReportTypeException.class, () -> {
            service.createReportByConference(1L, talkDTO);
        });
    }

    private TalkDTO createValidTalk() {
        TalkDTO talkDTO = new TalkDTO();
        talkDTO.setName("name");
        talkDTO.setReportType("REPORT");
        return talkDTO;
    }
}
