Basic service for creating conferences
-----------------
DockerFile and DockerCompose added

To run this service locally you should 
1. run command copy temp.env .env

2. fill in newly created .env  with your data

3. run command  docker-compose up -d 
